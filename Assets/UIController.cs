using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Canvas canvas;
    public InputController inputController;

    // Start is called before the first frame upda
    // Update is called once per frame

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
    }

    void Update()
    {
        if (inputController.mouseEnabled)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
      
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
     
        }
    }
}
