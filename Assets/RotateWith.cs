using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateWith : MonoBehaviour
{
    public GameObject rotationObject;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = rotationObject.transform.rotation;
    }
}
