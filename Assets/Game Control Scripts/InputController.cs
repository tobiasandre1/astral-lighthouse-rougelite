using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputController : MonoBehaviour
{
    public bool active = true;

    

    public float lookHorizontalSensitivity;
    public float lookVerticalSensitivity;

    public float moveLeft;
    public float moveRight;
    public float moveForward;
    public float moveBackward;

    public float lookRight;
    public float lookLeft;
    public float lookUp;
    public float lookDown;

    public float scrollFactor;

    public bool jump;
    public bool sprint;
    public bool crouch;
    public bool slide;

    public bool toggleCrouch = true;
    public bool toggleSprint = false;
    public bool toggleSlide = false;

    public bool mouseEnabled = false;
    

    public void Start()
    {
        Reset();
    }

    public void Update()
    {
        mouseEnabled = updateValueOfInputKey("Fire2", mouseEnabled, true);
    }

    public void Reset()
    {


        jump = false;
        sprint = false;
        crouch = false;
        slide = false;

        lookHorizontalSensitivity = 100f;
        lookVerticalSensitivity = 100f;

        moveLeft = 0f;
        moveRight = 0f;
        moveForward = 0f;
        moveBackward = 0f;

        lookRight = 0f;
        lookLeft = 0f;
        lookUp = 0f;
        lookDown = 0f;

        scrollFactor = 0;
    }

    public bool updateValueOfInputKey(string buttonName, bool currentValue, bool shouldToggle)
    {
        if (!shouldToggle)
        {
            return Input.GetButton(buttonName);
        }
        else if (Input.GetButtonDown(buttonName))
        {
            return !currentValue;
        }
        return currentValue;
    }

    public bool updateValueOfInputKeyThatDependsOnAnotherInput(string buttonName, bool currentValue, bool shouldToggle, bool dependingButtonIsActive)
    {
        if (dependingButtonIsActive)
        {
            return false;
        }
        return updateValueOfInputKey(buttonName, currentValue, shouldToggle);
    }
}
