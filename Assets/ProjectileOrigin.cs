using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileOrigin : MonoBehaviour
{
    public float distanceInFrontOfGameEntity = 0.5f;
    public GameObject followGameObject;

    public void Update()
    {
        transform.rotation = followGameObject.transform.rotation;
        transform.position = followGameObject.transform.position + distanceInFrontOfGameEntity * transform.forward;

    }


}
