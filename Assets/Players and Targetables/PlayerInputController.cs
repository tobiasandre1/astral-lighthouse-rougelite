using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : InputController
{

    void Update()
    {
        if (!active)
        {
            Reset();
            return;
        }

        scrollFactor = Mathf.Clamp(Input.mouseScrollDelta.y*0.1f + scrollFactor, -1, 1);
        
        jump = Input.GetButtonDown("Jump");
        slide = updateValueOfInputKey("Slide", slide, toggleSlide);
        sprint = updateValueOfInputKeyThatDependsOnAnotherInput("Sprint", sprint, toggleSprint, slide);
        crouch = updateValueOfInputKeyThatDependsOnAnotherInput("Crouch", crouch, toggleCrouch, sprint);
        

        moveForward = Input.GetAxis("Vertical");
        moveBackward = -moveForward;
        moveLeft = Input.GetAxis("Horizontal");
        moveRight = -moveLeft;

        lookLeft = Input.GetAxis("Mouse X");
        lookRight = -lookLeft;
        lookUp = Input.GetAxis("Mouse Y");
        lookDown = -lookUp;


    }
}
