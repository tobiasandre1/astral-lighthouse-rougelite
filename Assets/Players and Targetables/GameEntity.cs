using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEntity : MonoBehaviour
{

    public Slider healthBar;

    public float speed = 1;
    public float health = 100;
    public float MAX_HEALTH = 100;

    void Start()
    {
        if(healthBar != null)
        {
            healthBar.maxValue = MAX_HEALTH;
            healthBar.value = health;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ApplyDamage(float damage)
    {
        health -= damage;
        Debug.Log("I took " + damage + " points of damage!");
        if(healthBar != null) healthBar.value = health;
        
    }


}
