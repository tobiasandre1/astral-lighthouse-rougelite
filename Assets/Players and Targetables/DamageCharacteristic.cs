using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCharacteristic : MonoBehaviour
{
    public GameEntity connectedEntity;
    public float damageMultiplier = 1f;
   
    public void ApplyDamage(float damage)
    {
        connectedEntity.ApplyDamage(damageMultiplier * damage);
    }
}
