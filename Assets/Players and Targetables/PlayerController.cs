using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Collider collider;
    public Vector3 velocity = Vector3.zero;

    public Camera playerCamera;
    public Transform playerTransform;
    public CharacterController characterController;

    public GameEntity playerGameEntity;
    public GameObject velocityGameObject;

    public InputController playerInputController;

    public Animator animator;

    public float distance = 4.0f;
    public float distanceOffset = 2f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private float minimumCameraYAngle = -30f;
    private float maximumCameraYAngle = 87f;
    private float timeSinceGrounded = 0f;
    private float coyoteTime = 0.1f;
    private float baseAcceleration = 0.8f;
    private float slideAcceleration = 0f;
    private float brake = 0.01f;
    private float airControl = 0.2f;
    private float slideControl = 0.05f;
    private float slideTopSpeed = 300f;
    private float baseTopSpeed = 15f;
    private float topSpeed;
    private float sprintTopSpeedMultiplier = 3f;
    private float crouchTopSpeedMultiplier = 0.5f;
    private float gravity = 90f;
    private float jumpStrength = 40f;

    private bool fire = false;
    
    private float cameraOffset = 1.5f;
    private float baseCameraLag = 0.9f;
    private Vector3 cameraLag = Vector3.zero;
    
    private Vector3 relativeCameraPosition;
    public GameObject projectilePointOfOrigin;

    // animation IDs
    private int _animIDSpeed;
    private int _animIDGrounded;
    private int _animIDJump;
    private int _animIDFreeFall;
    private int _animIDMotionSpeed;
    private float _animationBlend;

    public Animator _animator;

    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
    }


    public void Start()
    {
        GameObject ui = GameObject.Find("UI Controller");
        ui.GetComponent<UIController>().canvas.worldCamera = playerCamera;
        topSpeed = baseTopSpeed;
        AssignAnimationIDs();
       
        
    }

    void Update()
    {

        if (fire)
        {
            RaycastHit raycastHit;
            Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out raycastHit);

            try
            {
                raycastHit.collider.GetComponentInParent<DamageCharacteristic>().ApplyDamage(100 * Time.deltaTime);
            }
            catch (NullReferenceException e)
            {

            }

        }
        UpdateAnimation();
        UpdateCamera();
        UpdateMovement();
        UpdateWeapons();
        UpdateAbilities();
    }


    public void FixedUpdate()
    {
        
        
    }

    public void LateUpdate()
    {
        
    }

    void UpdateAnimation()
    {

        Vector3 horizontalVelocity = new Vector3(velocity.x, 0, velocity.z);

        _animationBlend = Mathf.Lerp(_animationBlend, horizontalVelocity.magnitude, Time.deltaTime * 15f);
        _animator.SetFloat(_animIDSpeed, horizontalVelocity.magnitude/1.5f);
        _animator.SetFloat(_animIDMotionSpeed, Mathf.Clamp(horizontalVelocity.magnitude/15f, 1.5f, 10f));

        /*
        if (!characterController.isGrounded)
        {
            _animator.SetBool(_animIDFreeFall, true);
            _animator.SetBool(_animIDJump, false);
        }
        else if (playerInputController.jump)
        {
            _animator.SetBool(_animIDJump, true);
        }*/
        
        
        
    }


    void UpdateCamera()
    {

        currentX += playerInputController.lookLeft * 0.02f * playerInputController.lookHorizontalSensitivity;
        currentY -= playerInputController.lookUp * 0.02f * playerInputController.lookVerticalSensitivity;

        currentY = Mathf.Clamp(currentY, minimumCameraYAngle, maximumCameraYAngle);



        Vector3 dir = new Vector3(0, 0, -(distance + playerInputController.scrollFactor*distanceOffset));
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);

        playerCamera.transform.position = transform.position + rotation * dir;

        playerCamera.transform.LookAt(transform.position + new Vector3(0, 0.5f, 0));

        
        relativeCameraPosition = new Vector3(transform.position.x - playerCamera.transform.position.x, 0, transform.position.z - playerCamera.transform.position.z);
        transform.rotation = Quaternion.LookRotation(relativeCameraPosition);


        float velocityRatio = velocity.magnitude / topSpeed;



        Vector3 nextCameraLag;

        if (velocity.magnitude > 5f)
        {
            nextCameraLag = (velocity.normalized * baseCameraLag);
        } 
        else
        {
            nextCameraLag = Vector3.zero;
        }
            

        Vector3 cameraLagVelocity = (nextCameraLag-cameraLag)*5f;
        Vector3 modifiedCameraLag = Vector3.SmoothDamp(cameraLag, nextCameraLag, ref cameraLagVelocity, 0.02f);



        cameraLag = Vector3.ClampMagnitude(modifiedCameraLag, baseCameraLag);




        Vector3 offSet = playerCamera.transform.right * cameraOffset - cameraLag;
       
        

        playerCamera.transform.position += offSet;

        Vector3 velocityObjectChange = new Vector3(velocity.x, 0, velocity.z).normalized * 60f;

        if(velocityObjectChange.magnitude > 50f) velocityGameObject.transform.position = velocityObjectChange + transform.position;

    }

    void UpdateMovement()
    {
        velocity = CalculateMovement();
        characterController.Move(velocity * Time.deltaTime);
    }

    void UpdateWeapons()
    {
        fire = Input.GetButton("Fire1");
        
    }

    void UpdateAbilities()
    {

    }

    public Vector3 CalculateMovement()
    {

        topSpeed = baseTopSpeed;
        float acceleration = baseAcceleration;

        if (playerInputController.slide)
        {
            topSpeed = slideTopSpeed;
            acceleration = slideAcceleration;
        }
        else if (playerInputController.sprint)
        {
            topSpeed = baseTopSpeed * sprintTopSpeedMultiplier;
        }
        else if (playerInputController.crouch)
        {
            topSpeed = baseTopSpeed * crouchTopSpeedMultiplier;
        } 


        timeSinceGrounded = characterController.isGrounded ? 0.0f : timeSinceGrounded + Time.deltaTime;
        bool isGrounded = timeSinceGrounded < coyoteTime;

        Vector3 horizontalVelocity = new Vector3(velocity.x, 0, velocity.z);
        float horizontalMagnitude = horizontalVelocity.magnitude;

        Vector3 verticalVelocity = new Vector3(0, velocity.y, 0);
        float verticalMagnitide = verticalVelocity.magnitude;

        Vector3 inputHor = transform.TransformDirection(new Vector3(playerInputController.moveLeft, 0, playerInputController.moveForward).normalized) * acceleration;
        Vector3 inputVert = new Vector3(0, playerInputController.jump && isGrounded ? jumpStrength : 0, 0);
        

        Vector3 vGravity = new Vector3(0, isGrounded ? 0 : -gravity * Time.deltaTime, 0);

        Vector3 counterForce = Mathf.Min((acceleration - brake) / topSpeed * horizontalMagnitude + brake, horizontalMagnitude) * -horizontalVelocity.normalized;

        bool characterIsInAir = !isGrounded;
        bool characterIsSliding = playerInputController.slide;

        if (characterIsInAir)
        {
            inputHor *= airControl;
            counterForce *= airControl;
        } 
        else if (characterIsSliding)
        {
            inputHor *= slideControl;
            counterForce *= airControl;
        }

        horizontalVelocity += counterForce + inputHor;
        verticalVelocity += vGravity + inputVert;


        if (verticalVelocity.y < 0f && isGrounded) 
        { 
            verticalVelocity = Vector3.zero; 
        }

      

        return horizontalVelocity + verticalVelocity;
    }

}
